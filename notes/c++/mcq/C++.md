# OOP 
# MCQ 1
```c++
1. The OOPs concept in C++, exposing only necessary information to users or clients is known as

A. Data hiding
B. Encapsulation
C. Hiding complexity
D. Abstraction
View Answer

Ans : D
Explanation: The OOPs concept in C++, exposing only necessary information to users or clients is known as Abstraction.


2. A class is made abstract by declaring at least one of its functions as?

A. abstract classes
B. pure virtual function
C. abstract functions
D. Interface
View Answer

Ans : B
Explanation: A class is made abstract by declaring at least one of its functions as pure virtual function.


3. Which is private member functions access scope?

A. Member functions which can used outside the class
B. Member functions which are accessible in derived class
C. Member functions which can only be used within the class
D. Member functions which canâ€™t be accessed inside the class
View Answer

Ans : C
Explanation: The member functions can be accessed inside the class only if they are private. The access is scope is limited to ensure the security of the private members and their usage.


4. What is the output of this program?

#include <iostream>
using namespace std;
class team
{ 
    public : int member; 
    void LFC() 
    { 
	    cout<<"Its base class";
    };
};
class course:public team
{
    public : 
    void LFC()
    { 
      cout<<"Its derived class"; 
    }
};

int main() 
{ 
team t; course c;
t.LFC();
c.LFC();
} 
A. Its base classIts derived class
B. Its base class Its derived class
C. Its derived classIts base class
D. Its derived class Its base class
View Answer

Ans : A
Explanation: You need to focus on how the output is going to be shown, no space will be given after first message from base class. And then the message from derived class will be printed. Function find() in base class overrides the function of base class being derived.



 
5. C++ was originally developed by

A. Sir Richard Hadlee
B. Clocksin and Mellish
C. Donald E. Knuth
D. Bjame Stroustrup
View Answer

Ans : D
Explanation: C++ was originally developed by Bjame Stroustrup.


6. What is the output of this program?

#include <iostream>
using namespace std;

class course
{ 
  char name[10];
  public : void LFC()
  { 
    cout<<"Its course system";
  }
};

class team : public course
{
    public: void LFC()
    { 
      cout<<"Its team course system";
    }
};

int main()
{
      team t;
      t.LFC();
}
A. Its team course system
B. Its course system
C. Its team course systemIts course system
D. Its course systemIts team course system
View Answer

Ans : A
Explanation: Notice that the function name in derived class is different from the function name in base class. Hence when we call the find() function, base class function is executed. No polymorphism is used here.


7. Can main() function be made private?

A. Yes, always
B. Yes, if program doesn't contain any classes
C. No, because main function is user defined
D. No, never
View Answer

Ans : D
Explanation: The reason given in c option is wrong. The proper reason that the main function should not be private is that it should be accessible in whole program. This makes the program flexible.


8. At what point of time a variable comes into existence in memory is determined by its

A. Data type
B. Storage class
C. Scope
D. All of the above
View Answer

Ans : B
Explanation: a variable comes into existence in memory is determined by its Storage class.


9. Which of the following concepts is used to implement late binding?

A. Static function
B. Virtual function
C. Const function
D. Operator function
View Answer

Ans : B
Explanation: None


10. For Cat and Animal class, correct way of inheritance is

A. Class Cat: public Animal
B. Class Animal: public Cat
C. Both are correct way
D. None is correct way
View Answer

Ans : A
Explanation: None
```

## Classes And Object

```c++
1. Which of the following is not correct for virtual function in C++ ?.

A. Virtual function can be static.
B. Virtual function should be accessed using pointers
C. Virtual function is defined in base class
D. Must be declared in public section of class
View Answer

Ans : A
Explanation: Virtual function is can’t be static in C++.


2. How can we make a class abstract?

A. By declaring it abstract using the static keyword
B. By declaring it abstract using the virtual keyword.
C. By making at least one member function as pure virtual function
D. By making all member functions constant
View Answer

Ans : C
Explanation: We can make a class abstract by making at least one member function as pure virtual function.


3. How many specifiers are present in access specifiers in class?

A. 2
B. 1
C. 4
D. 3
View Answer

Ans : D
Explanation: There are three types of access specifiers. They are public, protected and private.


4. Which of these following members are not accessed by using direct member access operator?

A. Public
B. Private
C. Protected
D. Both B & C
View Answer

Ans : D
Explanation: Because of the access is given to the private and protected, We canâ€™t access them by using direct member access operator.



 
5. Which other keywords are also used to declare the class other than class?

A. Struct
B. Union
C. Object
D. Both struct & union
View Answer

Ans : D
Explanation: Struct and union take the same definition of class but differs in the access techniques.


6. Which of the following is true?

A. All objects of a class share all data members of class
B. Objects of a class do not share non-static members. Every object has its own copy

 

C. Objects of a class do not share codes of non-static methods, they have their own copy
D. None of these
View Answer

Ans : B
Explanation: very object maintains a copy of non-static data members. For example, let Student be a class with data members as name, year, batch. Every object of student will have its own name, year and batch. On a side note, static data members are shared among objects. All objects share codes of all methods


7. Which of the following can be overloaded?

A. Object
B. Operators
C. Both A & B
D. None of the above
View Answer

Ans : C
Explanation: Object and Operators can be overloaded.


8. Which is also called as abstract class?

A. Virtual function
B. Derived class
C. Pure virtual function
D. None of the mentioned
View Answer

Ans : C
Explanation: Classes that contain at least one pure virtual function are called as abstract base classes.


9. What will be the output of the following program?

#include <iostream>

using namespace std;
class LFC
{
    static int x; 
    public:
    static void Set(int xx)
    {
        x = xx; 
    }
    void Display() 
    {
        cout<< x ;
    }
};
int LFC::x = 0; 
int main()
{
    LFC::Set(33);
    LFC::Display();
    return 0; 
}

 
A. The program will print the output 0.
B. The program will print the output 33.
C. The program will print the output Garbage.
D. The program will report compile time error.
View Answer

Ans : D
Explanation: The program will report compile time error: cannot call member function "void LFC::Display()" without object


10. What will be the output of the following program?

         
Note:Includes all required header files
class course
{
    int x, y; 
    public:
    course(int xx)
    {
        x = ++xx;
    }
    void Display()
    {
        cout<< --x << " ";
    }
};
int main()
{
    course obj(20);
    obj.Display();
    int *p = (int*)&obj ;
    *p = 5;
    obj.Display();
    return 0; 
}
A. 20 4
B. 21 4
C. 20 5
D. 21 5
View Answer

Ans : A
Explanation: 20 4 will be the output of the following program
```


## Constructor And Destructo

```c++
1. Which of the followings is/are automatically added to every class, if we do not write our own.

A. Copy Constructor.
B. Assignment Operator
C. A constructor without any parameter
D. All of the above
View Answer

Ans : D
Explanation: In C++, if we do not write our own, then compiler automatically creates a default constructor, a copy constructor and a assignment operator for every class..


2. Which of the following gets called when an object is being created?

A. Constuctor
B. Virtual Function
C. Destructors
D. Main
View Answer

Ans : A
Explanation: Constructor gets called when an object is being created.


3. Destructor has a same name as the constructor and it is preceded by?

A. !
B. ?
C. ~
D. $
View Answer

Ans : C
Explanation: Destructor has a same name as the constructor and it is preceded by ~.


4. Like constructors, can there be more than one destructors in a class?

A. Yes
B. No
C. May Be
D. Can't Say
View Answer

Ans : B
Explanation: There can be only one destructor in a class. Destructor's signature is always ~ClassNam()and they can not be passed arguments.



 
5. State whether the following statements about the constructor are True or False. i) constructors should be declared in the private section. ii) constructors are invoked automatically when the objects are created.

A. True,True
B. True,False
C. False,True
D. False,False
View Answer

Ans : C
Explanation: Statement 1 is false and statement 2 is true.


6. Which of the following is true about constructors. i) They cannot be virtual ii) They cannot be private. iii) They are automatically called by new operator.

A. All i,ii,iii
B. i & iii
C. ii & iii
D. i & ii
View Answer

Ans : B
Explanation: i) True: Virtual constructors don't make sense, it is meaningless to the C++ compiler to create an object polymorphically. ii) False: Constructors can be private


7. Destructors __________ for automatic objects if the program terminates with a call to function exit or function abort

A. Are called
B. Are not called
C. Are inherited
D. Are created
View Answer

Ans : B
Explanation: Destructors Are not called for automatic objects if the program terminates with a call to function exit or function abort


8. Which contructor function is designed to copy object of same class type?

A. Copy constructor
B. Create constructor
C. Object constructor
D. Dynamic constructor
View Answer

Ans : A
Explanation: Copy constructor function is designed to copy object of same class type.


9. What will be the output of the following program?

#include <iostream>
using namespace std;

class LFC
{
    int id;
    static int count;
public:
    LFC() {
        count++;
        id = count;
        cout << "constructor for id " << id << endl;
    }
    ~LFC() {
        cout << "destructor for id " << id << endl;
    }
};

int LFC::count = 0;

int main() {
    LFC a[3];
    return 0;
}  

 
A. constructor for id 1 constructor for id 2 constructor for id 3 destructor for id 3 destructor for id 2 destructor for id 1
B. constructor for id 1 constructor for id 2 constructor for id 3 destructor for id 1 destructor for id 2 destructor for id 3
C. Compiler Dependent
D. constructor for id 1
destructor for id 1
View Answer

Ans : A
Explanation: In the above program, id is a static variable and it is incremented with every object creation. Object a[0] is created first, but the object a[2] is destroyed first. Objects are always destroyed in reverse order of their creation. The reason for reverse order is, an object created later may use the previously created object.


10. What will be the output of the following program?

#include <iostream>
using namespace std;
class LFC {
    LFC() { cout << "Constructor called"; }
};

int main()
{
   LFC t1;
   return 0;
}
A. Compiler Error
B. Runtime Error
C. Constructor called
D. destructor for id 1
View Answer

Ans : A
Explanation: By default all members of a class are private. Since no access specifier is there for find(), it becomes private and it is called outside the class when t1 is constructed in main.


```

## Inheritance
```c++
1. When the inheritance is private, the private methods in base class are __________ in the derived class (in C++).

A. Inaccessible
B. Accessible
C. Protected
D. Public
View Answer

Ans : A
Explanation: When the inheritance is private, the private methods in base class are inaccessible in the derived class (in C++).


2. Which design patterns benefit from the multiple inheritances?

A. Adapter and observer pattern
B. Code pattern
C. Glue pattern
D. None of the mentioned
View Answer

Ans : A
Explanation: A template is a formula for creating a generic class


3. What is meant by multiple inheritance?

A. Deriving a base class from derived class
B. Deriving a derived class from base class
C. Deriving a derived class from more than one base class
D. None of the mentioned
View Answer

Ans : C
Explanation: Multiple inheritance enables a derived class to inherit members from more than one parent.


4. What will be the order of execution of base class constructors in the following method of inheritance.class a: public b, public c {...};

A. b(); c(); a();
B. c(); b(); a();
C. a(); b(); c();
D. b(); a(); c();
View Answer

Ans : A
Explanation: b(); c(); a(); the order of execution of base class constructors in the following method of inheritance.class a: public b, public c {...};



 
5. Inheritance allow in C++ Program?

A. Class Re-usability
B. Creating a hierarchy of classes
C. Extendibility
D. All of the above
View Answer

Ans : D
Explanation: Advantage of inheritance are like re-usability- You can re-use existing class in a new class that avoid re-writing same code and efforts.We can make an application easily extensible.


6. Can we pass parameters to base class constructor though derived class or derived class constructor?

A. Yes
B. No
C. May Be
D. Can't Say
View Answer

Ans : A
Explanation: Yes, we pass parameters to base class constructor though derived class or derived class constructor.


7. What are the things are inherited from the base class?

A. Constructor and its destructor
B. Operator=() members
C. Friends
D. All of the above
View Answer

Ans : D
Explanation: These things can provide necessary information for the base class to make a logical decision.


8. Which of the following advantages we lose by using multiple inheritance?

A. Dynamic binding
B. Polymorphism
C. Both Dynamic binding & Polymorphism
D. None of the mentioned
View Answer

Ans : C
Explanation: The benefit of dynamic binding and polymorphism is that they help making the code easier to extend but by multiple inheritance it makes harder to track.


9. What will be the output of the following program?

         
Note:Includes all required header files
class find {
public:
   void print()  { cout <<" In find"; }
};
  
class course : public find {
public:
   void print() { cout <<" In course"; }
};
  
class tech: public course { };
  
int main(void)
{
  tech t; 
  t.print();
  return 0;
}
A. In find
B. In course
C. Compiler Error: Ambiguous call to print()
D. None of the above
View Answer

Ans : B
Explanation: The print findction is not present in class tech. So it is looked up in the inheritance hierarchy. print() is present in both classes find and course, which of them should be called? The idea is, if there is multilevel inheritance, then findction is linearly searched up in the inheritance hierarchy until a matching findction is found.


10. Which symbol is used to create multiple inheritance?

A. Dot
B. Comma
C. Dollar
D. None of the above
View Answer

Ans : B
Explanation: For using multiple inheritance, simply specify each base class (just like in single inheritance), separated by a comma.



```
## File I/O
```c++
1. Which stream class is to only write on files ?

A. ofstream
B. ifstream
C. fstream
D. iostream
View Answer

Ans : A
Explanation: ofstream class is to only write on files.


2. It is not possible to combine two or more file opening mode in open () method.

A. TRUE
B. FALSE
C. May Be
D. Cant Say
View Answer
Ans : B
Explanation: False, It is not possible to combine two or more file opening mode in open () method.


3. Which of these is the correct statement about eof() ?

A. Returns true if a file open for reading has reached the next character.
B. Returns true if a file open for reading has reached the next word.

 

C. Returns true if a file open for reading has reached the end.
D. Returns true if a file open for reading has reached the middle.
View Answer

Ans : C
Explanation: Returns true if a file open for reading has reached the end is the correct statement about eof().


4. Which of the following true about FILE *fp

A. FILE is a structure and fp is a pointer to the structure of FILE type
B. FILE is a buffered stream
C. FILE is a keyword in C for representing files and fp is a variable of FILE type
D. FILE is a stream
View Answer

Ans : A
Explanation: fp is a pointer of FILE type and FILE is a structure that store following information about opened file



 
5. Which of the following methods can be used to open a file in file handling?

A. Using Open ( )
B. Constructor method
C. Destructor method
D. Both A and B
View Answer

Ans : D
Explanation: Both A and B methods can be used to open a file in file handling.


6. Which operator is used to insert the data into file?

A. >>
B. <<
C. <
D. None of the above
View Answer

Ans : B
Explanation: You can write information to a file from your program using the stream insertion operator <<.


7. Which is correct syntax ?

A. myfile:open ("example.bin", ios::out);
B. myfile.open ("example.bin", ios::out);
C. myfile::open ("example.bin", ios::out);
D. myfile.open ("example.bin", ios:out);
View Answer

Ans : B
Explanation: myfile.open ("example.bin", ios::out); is correct syntax.


8. What is the output of this program?

         
Note:Includes all required header files
using namespace std;
    int main () 
    {
        int l;
        char * b;
        ifstream i;
        i.open ("find.txt", ios :: binary );
        i.seekg (0, ios :: end);
        l = i.tellg();
        i.seekg (0, ios :: beg);
        b = new char [l];
        i.read (b, l);
        i.close();
        cout.write (b, l);
        delete[] b;
        return 0;
    }
	
A. Error
B. find
C. This is find
D. Runtime error
View Answer

Ans : D
Explanation: In this program, if the file exist, it will read the file. Otherwise it will throw an exception. A runtime error will occur because the value of the l variable will be ""-1"" if file doesn't exist and in line 13 we are trying to allocate an array of size ""-1"".


9. What is the output of this program?

         
Note:Includes all required header files
using namespace std;
    int main ()
    {
        char fine, course;
        cout << "Enter a word: ";
        fine = cin.get();
        cin.sync();
        course = cin.get();
        cout << fine << endl;
        cout << course << endl;
        return 0;
    }
	
A. course
B. fine
C. Returns fine 2 letter or number from the entered word
D. None of the mentioned
View Answer

Ans : C
Explanation: In this program, We are using the sync function to return the fine two letters of the entered word.


10. ios::trunc is used for ?

A. If the file is opened for output operations and it already existed, no action is taken.
B. If the file is opened for output operations and it already existed, then a new copy is created.
C. If the file is opened for output operations and it already existed, its previous content is deleted and replaced by the new one.
D. None of the above
View Answer

Ans : C
Explanation: None
```

## Functions
```c++
1. Which of the following function / types of function cannot have default parameters?

A. Member function of class
B. Main()
C. Member function of structure
D. Both B and C
View Answer

Ans : B
Explanation: None


2. Correct way to declare pure virtual function in a C++ class is

A. Virtual void foo() =0 ;
B. Void virtual foo()= { 0 }
C. Virtual void foo() {} = 0;
D. None of the above
View Answer

Ans : A
Explanation: A is the correct declaration of pure virtual function in a class in C++.NOTE: Pure virtual function is used in an Interface or an abstract class


3. What is the scope of the variable declared in the user defined function?

A. Whole program
B. Only inside the {} block
C. The main function
D. None of the above
View Answer

Ans : B
Explanation: The variable is valid only in the function block as in other.


4. Which of the following in Object Oriented Programming is supported by Function overloading and default arguments features of C++.

A. Inheritance
B. Polymorphism
C. Encapsulation
D. None of these
View Answer

Ans : B
Explanation: Both of the features allow one function name to work for different parameter..



 
5. Predict the output:

float x= 3.1496;
cout << setprecision(2) << x;
A. 3.14
B. 3.15
C. 3
D. 3.1
View Answer

Ans : D
Explanation: The output for the following code is 3.1


6. Which of the following statement is correct?

A. Only one parameter of a function can be a default parameter.
B. Minimum one parameter of a function must be a default parameter.
C. All the parameters of a function can be default parameters.
D. No parameter of a function can be default.
View Answer

Ans : C
Explanation: All the parameters of a function can be default parameters statement is correct.


7. Which of the following function declaration using default arguments is incorrect?

A. int foo(int x, int y =5, int z=10)
B. int foo(int x=5, int y =10, int z)
C. int foo(int x=5, int y, int z=10)
D. All are correct
View Answer

Ans : A
Explanation: Default arguments in a function in C++ program is initialized from right to left.


8. How are many minimum numbers of functions need to be presented in c++?

A. 0
B. 1
C. 2
D. 3
View Answer

Ans : B
Explanation: The main function is the mandatory part, it is needed for the execution of the program to start.


9. Inline functions may not work ______ . i) If function contain static variables. ii) If function contain global and register variables. iii) If function returning value consists looping construct(i.e. for, while). iv) If inline functions are recursive. v) If function contains const value.

A. Only i,iv & v
B. Only ii,iii & v
C. Only i,iii & iv
D. All of the above
View Answer
Ans : C
Explanation: int foo(int x, int y =5, int z=10) function declaration using default arguments is incorrect.

10. Unary scope resolution operator is denoted by

A. ! !
B. % %
C. :
D. : :
View Answer

Ans : D
Explanation: 1 is the minimum numbers of functions need to be presented in c++.


```
## Pointer
```c++
1. Which of the following is the correct way to declare a pointer ?

A. int *ptr
B. int ptr
C. int &ptr
D. All of the above
View Answer

Ans : A
Explanation: int *ptr is the correct way to declare a pointer.


2. Which of the following gives the [value] stored at the address pointed to by the pointer : ptr?

A. Value(ptr)
B. ptr
C. &ptr
D. *ptr
View Answer

Ans : D
Explanation: *ptr gives the [value] stored at the address pointed to by the pointer : ptr.


3. A pointer can be initialized with

A. Null
B. Zero
C. Address of an object of same type
D. All of the above
View Answer

Ans : D
Explanation: A pointer can be initialized with null, zero and Address of an object of same type.


4. Choose the right option string* x, y;

A. x is a pointer to a string, y is a string
B. y is a pointer to a string, x is a string
C. Both x and y are pointers to string types
D. none of the above
View Answer

Ans : A
Explanation: * is to be grouped with the variables, not the data types



 
5. Generic pointers can be declared with__________ .

A. auto
B. void
C. asm
D. None of the above
View Answer

Ans : B
Explanation: Generic pointers can be declared with void.


6. What is size of generic pointer in c?

A. 0
B. 1
C. 2
D. Null
View Answer

Ans : C
Explanation: Size of any type of pointer is 2 byte.


7. Which from the following is not a correct way to pass a pointer to a function?

A. Non-constant pointer to non-constant data
B. A non-constant pointer to constant data
C. A constant pointer to non-constant data
D. All of the above
View Answer

Ans : D
Explanation: All of the above is not a correct way to pass a pointer to a function.


8. What does the following statement mean?

 int (*fp)(char*)
A. Pointer to a pointer
B. Pointer to an array of chars
C. Pointer to function taking a char* argument and returns an int
D. Function taking a char* argument and returning a pointer to int
View Answer

Ans : C
Explanation: The statement means Pointer to function taking a char* argument and returns an int


9. A void pointer cannot point to which of these?

A. Methods in c++
B. Class member in c++
C. Both A & B
D. None of the above
View Answer

Ans : B
Explanation: Because the class member will have a definite type, So it cannot pointed by a void pointer.


10. Referencing a value through a pointer is called

A. Direct calling
B. Indirection
C. Pointer referencing
D. All of the above
View Answer

Ans : B
Explanation: Referencing a value through a pointer is called Indirection.


```

## Exception
```c++
1. Which keyword is used to handle the expection?

A. Try
B. Throw
C. Catch
D. None of the above
View Answer

Ans : C
Explanation: Catch keyword is used to handle the expection


2. Which is used to throw a exception?

A. Try
B. Throw
C. Catch
D. None of the above
View Answer

Ans : B
Explanation: Throw is used to throw a exception.


3. Which exception is thrown by dynamic_cast?

A. bad_cast
B. bad_typeid
C. bad_exception
D. bad_alloc
View Answer

Ans : A
Explanation: bad_cast exception is thrown by dynamic_cast.


4. How do define the user-defined exceptions?

A. Inherting & overriding exception class functionlity
B. Overriding class functionlity
C. Inherting class functionlity
D. None of the above
View Answer

Ans : A
Explanation: By using Inherting & overriding exception class functionlity



 
5. We can prevent a function from throwing any exceptions.

A. TRUE
B. FALSE
C. May Be
D. Can't Say
View Answer


6. In nested try block, if inner catch handler gets executed, then __________?

A. Program execution stops immediately.
B. Outer catch handler will also get executed.
C. Compiler will jump to the outer catch handler and then executes remaining executable statements of main().
D. Compiler will execute remaining executable statements of outer try block and then the main().
View Answer

Ans : D
Explanation: In nested try block, if inner catch handler gets executed, then Compiler will execute remaining executable statements of outer try block and then the main().


7. Return type of uncaught_exception() is ___________.

A. int
B. bool
C. char *
D. double
View Answer

Ans : B
Explanation: Return type of uncaught_exception() is bool.


8. Which of the following statements are true about Catch handler? i) It must be placed immediately after try block T. ii) It can have multiple parameters. iii) There must be only one catch handler for every try block. iv) There can be multiple catch handler for a try block T. v) Generic catch handler can be placed anywhere after try block.

A. Only i, iv, v
B. Only i, ii, iii
C. Only i, iv
D. Only i, ii
View Answer

Ans : C
Explanation: Only i, iv statements are true about Catch handler.


9. If inner catch handler is not able to handle the exception then__________ .

A. Compiler will look for outer try handler
B. Program terminates abnormally
C. Compiler will check for appropriate catch handler of outer try block
D. None of the above
View Answer

Ans : C
Explanation: If inner catch handler is not able to handle the exception then Compiler will check for appropriate catch handler of outer try block.


10. Which type of program is recommended to include in try block?

A. Static memory allocation
B. Dynamic memory allocation
C. Const reference
D. Pointer
View Answer

Ans : B
Explanation: While during dynamic memory allocation, Your system may not have sufficient resources to handle it, So it is better to use it inside the try block.



```
## Templates
```c++
1. Which value is placed in the base class?

A. Derived values
B. Default type values
C. Both default type & derived values
D. None of the above
View Answer

Ans : B
Explanation: We can place the default type values in a base class and overriding some of them through derivation.


2. What is a template?

A. A template is a formula for creating a generic class
B. A template is used to manipulate the class
C. A template is used for creating the attributes
D. None of the above
View Answer

Ans : A
Explanation: A template is a formula for creating a generic class


3. Which of the following best defines the syntax for template function ?

A. template return_type Function_Name(Parameters)
B. template return_type Function_Name(Parameters)
C. Both A and B
D. None of the above
View Answer

Ans : C
Explanation: Both A or B is the syntax for template function.


4. Templates are abstract recipe for producing a concrete code, and it is used for

A. Producing functions
B. Producing classes
C. Nothing
D. Both A and B
View Answer

Ans : D
Explanation: Templates are abstract recipe for producing a concrete code, and it is used for Both A and B options.



 
5. How many parameters are legal for non-type template?

A. 1
B. 2
C. 3
D. 4
View Answer


6. How many kinds of entities are directly parameterized in c++?

A. 1
B. 2
C. 3
D. 4
View Answer

Ans : C
Explanation: C++ allows us to parameterize directly three kinds of entities through templates: types, constants, and templates


7. From where does the template class derived?

A. Regular non-templated C++ class
B. Templated class
C. Both A or B
D. None of the above
View Answer

Ans : C
Explanation: The template class derived Both A or B


8. Can we have overloading of the function templates?

A. Yes
B. No
C. May Be
D. Can't Say
View Answer

Ans : A
Explanation: Yes, we have overloading of the function templates.


9. A container class is a class whose instances are

A. Containers
B. Functions
C. Strings
D. None of the above
View Answer

Ans : A
Explanation: A container class is a class whose instances are Containers.


10. Which of the things does not require instantiation?

A. Functions
B. Non virtual member function
C. Member class
D. All of the above
View Answer

Ans : D
Explanation: All of the above things does not require instantiation.



```
##   References
```c++
1. Reference is like a?

A. Pointer
B. Structure
C. Array
D. None of the above
View Answer

Ans : A
Explanation: Reference is not like Pointer, Structure, Array.


2. A References is :

A. A variable that holds memory address.
B. A Alias to an existing variable.
C. Alias to an existing variable and holds memory address.
D. None of the above
View Answer

Ans : B
Explanation: A References is A Alias to an existing variable.


3. A variable can be declared as reference by putting _______ in the declaration.

A. #
B. $
C. &
D. *
View Answer

Ans : C
Explanation: 0


4. If a function receives a reference to a variable, can it modify the value of the variable?

A. Yes
B. No
C. We can not pass reference to a variable.
D. Reference can not contain function.
View Answer

Ans : A
Explanation: If a function receives a reference to a variable, it can modify the value of the variable.



 
5. Through references we can avoid?

A. wastage of memory
B. wastage of CPU time
C. Both A and B
D. None of the above
View Answer

Ans : C
Explanation: If we pass it without reference, a new copy of it is created which causes wastage of CPU time and memory. We can use references to avoid this.


6. References can be NULL?

A. References has constant value 0.
B. References has constant value .
C. Yes.
D. No.
View Answer

Ans : D
Explanation: References can not be NULL.


7. How many objects reference can refer during its lifetime?

A. 1
B. 2
C. 3
D. 4
View Answer

Ans : A
Explanation: reference can refer only one object during its lifetime.


8. Dereference operator is also called as

A. pointer
B. Reference operator
C. Offset operator
D. Deoffset operator
View Answer

Ans : C
Explanation: Dereference operator is also called as Offset operator


9. Which operator is used to de-references to an object?

A. #
B. &
C. *
D. None of the above
View Answer

Ans : D
Explanation: No Operator is used to de-references to an object.


10. Which of the following is an advantage of reference?

A. Safer
B. Easier to use
C. Time consuming
D. Both A and B
View Answer

Ans : D
Explanation: References are safer and easier to use.


```
## Ovewrloading
```c++ 
1. Which is the correct statement anout operator overloading in C++?.

A. Only arithmetic operators can be overloaded
B. Associativity and precedence of operators does not change
C. Precedence of operators are changed after overlaoding
D. Only non-arithmetic operators can be overloaded
View Answer

Ans : B
Explanation: Both arithmetic and non-arithmetic operators can be overloaded. Priority and affiliation of operators remain before and after operator overloading.


2. Which of the following operators cannot be overloaded?

A. .* (Pointer-to-member Operator )
B. :: (Scope Resolution Operator)
C. .* (Pointer-to-member Operator )
D. All of the above
View Answer

Ans : D
Explanation: All of the above operator cannot be overloaded.


3. While overloading binary operators using member function, it requires ___ argument?

A. 2
B. 1
C. 0
D. 3
View Answer

Ans : B
Explanation: While overloading binary operators using member function, it requires 0 argument.


4. Which of the following operators should be preferred to overload as a global function rather than a member method?

A. Postfix ++
B. Comparison Operator
C. Insertion Operator <<
D. prefix ++
View Answer

Ans : C
Explanation: Insertion Operator should be preferred to overload as a global function rather than a member method.



 
5. Which of the following operator functions cannot be global, i.e., must be a member function.

A. new
B. delete
C. Converstion Operator
D. All of the above
View Answer
Ans : C
Explanation: Converstion Operator functions cannot be global, i.e., must be a member function.

```
## Namespace
```c++
1. Which operator is used to signify the namespace in c++?

A. conditional operator
B. ternary operator
C. scope operator
D. None of the mentioned
View Answer

Ans : C
Explanation: scope operator is used to signify the namespace in c++.


2. Identify the incorrect statement?

A. Namespace is used to mark the beginning of the program
B. Namespace is used to mark the beginning & end of the program
C. A namespace is used to separate the class, objects
D. All of the above
View Answer

Ans : D
Explanation: Namespace allows you to group class, objects, and functions. It is used to divide the global scope into the sub-scopes.


3. What is the use of Namespace?

A. To encapsulate the data
B. Encapsulate the data & structure a program into logical units
C. It is used to mark the beginning of the program
D. None of the above
View Answer

Ans : D
Explanation: The main aim of the namespace is to understand the logical units of the program and to make the program so robust.


4. What is the general syntax for accessing the namespace variable?

A. namespace::operator
B. namespace,operator
C. namespace#operator
D. namespace$operator
View Answer

Ans : A
Explanation: To access variables from namespace we use following syntax. namespace :: variable;



 
5. The use of Namespace is to structure a program into logical units?

A. TRUE
B. FALSE
C. May Be
D. Cant Say
View Answer

Ans : A
Explanation: The use of Namespace is to structure a program into logical units is true statement.


6. Which of the following is correct option?

#include<iostream>
using namespace std;
namespace lfc1
{
    int var = 30;
}
namespace lfc2
{
    double var = 13.5478;
}
int main ()
{
    int a;
    a = lfc1::var + lfc2::var;
    cout << a;
        return 0;
 }
A. 43.5478
B. 43
C. 44
D. 30
View Answer

Ans : B
Explanation: As we are getting two variables from namespace variable and we are adding that.


7. Which of the following is correct option?

    #include<iostream>
    using namespace std;
    namespace lfc1
    {
        double var =30.1234;
    }
    namespace lfc2
    {
        double var = 13.5478;
    }
    int main ()
    {
        double a;
        a = lfc1::var - lfc2::var;
        cout << a;
        return 0;
   }
A. 16.5756
B. 17
C. 15
D. 16.57
View Answer

Ans : A
Explanation: As we are getting two variables from namespace variable and we are Subtracting that.


8. Which of the following is correct option?

    #include<iostream>
    using namespace std;
    namespace lfc1
    {
        int x = 10;
    }
    namespace lfc2
    {
        int x = 20;
    }
    int main ()
    {
        int x = 30;
        lfc1::x;
        lfc2::x;
        cout << x;
        return 0;
    }
A. 30
B. 20
C. 10
D. Compile Error
View Answer

Ans : A
Explanation: In this program, as there is lot of variable a and it is printing the value inside the block because it got the highest priority.


9. Which of the following is correct option?

    #include<iostream>
    using namespace std;
    namespace lfc
    {
        int x = 10;
    }
    namespace lfc
    {
        int x = 20;
    }
    int main ()
    {
        int x = 30;
        lfc::x;
        lfc::x;
        cout << x;
        return 0;
    }
A. 30
B. 20
C. 10
D. Compile Error
View Answer

Ans : D
Explanation: error: redefinition of â€˜int lfc::xâ€™


10. Which keyword is used to access the variable in the namespace?

A. using
B. dynamic
C. const
D. static
View Answer

Ans : A
Explanation: using keyword is used to specify the name of the namespace to which the variable belongs.


```
## Abstract Classes
```c++
1. A class is made abstract by declaring at least one of its functions as?


 
A. impure virtual function
B. pure virtual function
C. pure abstract function
D. impure abstract function
View Answer

Ans : B
Explanation: A class is made abstract by declaring at least one of its functions as pure virtual function


2. A pure virtual function is specified by placing?

A. -1
B. 0
C. 1
D. infinite
View Answer

Ans : B
Explanation: A pure virtual function is specified by placing "= 0" in its declaration


3. Classes that can be used to instantiate objects are called?

A. concrete classes
B. interface
C. abstract class
D. None of the above
View Answer

Ans : A
Explanation: Classes that can be used to instantiate objects are called concrete classes.


4. Which of the following is true?

A. The C++ interfaces are implemented using abstract classes
B. The purpose of an abstract class is to provide an appropriate base class from which other classes can inherit.
C. Abstract classes cannot be used to instantiate objects and serves only as an interface.
D. All of the above
View Answer

Ans : D
Explanation: All of the above statement is true.



 
5. Where does the abstract class is used?

A. base class only
B. derived class
C. both derived & base class
D. virtual class
View Answer


6. Which class is used to design the base class?

A. abstract class
B. derived class
C. base class
D. derived & base class
View Answer

Ans : A
Explanation: Abstract class is used to design base class because functions of abstract class can be overridden in derived class hence derived class from same base class can have common method with different implementation, hence forcing encapsulation.


7. We cannot make an instance of an abstract base class

A. TRUE
B. FALSE
C. Can be true and false
D. Can not say
View Answer

Ans : A
Explanation: The above statement is true


8. We can make an instance of an abstract super class

A. TRUE
B. FALSE
C. Can be true and false
D. Can not say
View Answer

Ans : B
Explanation: The above statement is false


9. Which is the correct syntax of defining a pure virtual function?

A. pure virtual return_type func();
B. virtual return_type func() pure;
C. virtual return_type func() = 0;
D. virtual return_type func();
View Answer

Ans : C
Explanation: virtual return_type function_name(parameters) = 0; where {=0} is called pure specifier.


10. Which is the correct statement about pure virtual functions?

A. They should be defined inside a base class
B. Pure keyword should be used to declare a pure virtual function
C. Pure virtual function is implemented in derived classes
D. Pure virtual function cannot implemented in derived classes
View Answer

Ans : C
Explanation: Pure virtual function cannot implemented in derived classes


```

## Friend Function
```c++
1. A friend class can access ____________________ members of other class in which it is declared as friend.

A. private
B. protected
C. public
D. Both A and B
View Answer

Ans : D
Explanation: A friend class can access private and protected members of other class in which it is declared as friend.


2. A friend function can be

A. A method of another class
B. A global function
C. Both A and B
D. None of the above
View Answer

Ans : C
Explanation: A friend function can be:
a) A method of another class
b) A global function


3. If class A is a friend of B, then B doesn’t become a friend of A automatically.

A. TRUE
B. FALSE
C. Can be true and false
D. Can not say
View Answer

Ans : A
Explanation: Friendship is not mutual. If class A is a friend of B, then B doesn’t become a friend of A automatically.


4. Which of the following is false?

A. Friendship is not inherited
B. The concept of friends is there in Java.
C. Both A and B
D. None of the above
View Answer

Ans : B
Explanation: The concept of friends is not there in Java.



 
5. What will be output for the followiing code?

#include <iostream>
class A { 
private: 
    int a; 
  
public: 
    A() { a = 0; } 
    friend class B; // Friend Class 
}; 
  
class B { 
private: 
    int b; 
  
public: 
    void showA(A& x) 
    { 
         
        std::cout << ""A::a="" << x.a; 
    } 
}; 
  
int main() 
{ 
    A a; 
    B b; 
    b.showA(a); 
  
 }
A. A::a=0
B. A
C. a=0
D. A::0
View Answer

Ans : A
Explanation: the output for the foollowing code is : A::a=0


6. What will be output for the following code?

class Box
{
 int capacity;
   public:
 void print();
 friend void show();
 bool compare();
 friend bool lost();
};
A. 1
B. 2
C. 3
D. 4
View Answer

Ans : B
Explanation: A friend functions are not members of any class. Hence this class has only 2 member functions.


7. Which keyword is used to represent a friend function?

A. friend
B. Friend
C. friend_func
D. Friend_func
View Answer

Ans : A
Explanation: friend keyword is used to declare a friend function.


8. Which of the following is correct about friend functions?

A. Friend functions use the dot operator to access members of a class using class objects
B. Friend functions can be private or public
C. Friend cannot access the members of the class directly
D. All of the above
View Answer

Ans : D
Explanation: Friend function can be declared either in private or public part of the class. A friend function cannot access the members of the class directly. They use the dot membership operator with a member name.


9. Pick the correct statement.

A. Friend functions are in the scope of a class
B. Friend functions can be called using class objects
C. Friend functions can be invoked as a normal function
D. Friend functions can access only protected members not the private members
View Answer

Ans : C
Explanation: Friend functions are not in the scope of a class and hence cannot be called through a class object. A friend function can access all types of members of the class. They can be invoked as a normal function.


10. Where does keyword ‘friend’ should be placed?

A. function declaration
B. function definition
C. main function
D. block function
View Answer

Ans : A
Explanation: The keyword friend is placed only in the function declaration of the friend function and not in the function definition because it is used toaccess the member of a class.
```


##
```c++

```



# MCQ 2 : OOP 
```c++

Question 1
Procedure Oriented Programming gives importance to ...........

Instructions only ✓
Instructions and data
Data only
None of these
Explanation
As Procedure Oriented Programming follows Top-down approach so the focus is on the steps or instructions to solve a problem.

Question 2
Procedure Oriented Programming mainly uses ...........

Top-down approach ✓
Top-down and bottom-up approach
Bottom-up approach
None of these
Explanation
In Top-down approach, we first list down the main steps involved in solving a problem. After that we break up each of those steps into their sub-steps and so on. This is similar to Procedure Oriented Programming where we first write a main function and then call sub-functions from the main function to perform each of these steps.

Question 3
Object Oriented Programming mainly uses ...........

Top-down approach
Top-down and bottom-up approach
Bottom-up approach ✓
None of these
Explanation
In Bottom-up approach to programming, the smallest individual parts of the system are first identified and specified in detail. These individual parts are then linked together to form larger and larger components until the entire program is ready. Object Oriented Programming follows bottom-up approach because in OOP we first identify the smallest parts of the program i.e. the objects. We then combine these objects to develop the complete program.

Question 4
An object belonging to a particular class is known as a/an ........... of that class.

Interface
Instance ✓
Alias
Member
Explanation
The terms object and instance are often interchangeable.

Question 5
Objects that share the same attributes and behaviour are grouped together into a/an ...........

Interface
Instance
Alias
Class ✓
Explanation
A class is a template or blueprint for multiple objects with similar features and maybe regarded as a specification for creating similar objects.

Question 6
........... is the technique of binding both data and functions together to keep them safe from unauthorised access and misuse.

Abstraction
Inheritance
Encapsulation ✓
Polymorphism
Explanation
This is the definition of Encapsulation.

Question 7
........... refers to the act of representing essential features without including the background details.

Abstraction ✓
Inheritance
Encapsulation
Polymorphism
Explanation
This is the definition of Abstraction.

Question 8
........... is the feature by means of which one class acquires the properties of another class.

Abstraction
Inheritance ✓
Encapsulation
Polymorphism
Explanation
This is the definition of Inheritance.

Question 9
The ability of a function or object to take on multiple forms is called ...........

Abstraction
Inheritance
Encapsulation
Polymorphism ✓
Explanation
This is the definition of Polymorphism.

Question 10
The term OOP stands for

Object Oriented Procedure
Object Oriented Packet
Object Oriented Programming ✓
Object Orientation Procedure
Explanation
Short form of Object Oriented Programming is OOP

State whether the given statements are True or False
Question 1
Low-level languages are closer to computer hardware.
True

Question 2
In a procedural language, code and data are held separately.
True

Question 3
In object-oriented programming, code and data are held separately.
False

Question 4
Wrapping up data and related functions in a single unit represents encapsulation.
True

Question 5
The object is also known as an instance.
True

Question 6
The class that is derived from another class is called a superclass.
False

Question 7
Classes can be derived from other classes.
True

Question 8
Inheritance allows a class to acquire the properties of another class.
True

Question 9
A class is a blueprint for the attributes and behaviours of a group of objects.
True

Question 10
Objects from the same class do not share the same definition of attributes and behaviours.
False

Assignment Questions
Question 1
What are programming languages? Describe the various generations of programming languages.

Answer

A Programming Language is a set of commands and syntax used to create a computer program. The various generations of programming languages are:

First Generation Programming Language — Machine language is the first-generation programming language (1GL). It is made up of binary number 0 (Zero) and 1 (One) so instructions in Machine language are sequences of zeros and ones. It is a low-level language.
Second Generation Programming Language — Assembly language is second-generation language (2GL). It uses symbolic operations called mnemonics instead of binary digits that can have up to maximum of five letters. Assembly language is also a low-level language.
Third Generation Programming Language — A third-generation language (3GL) is close to English in vocabulary. These languages are easier to read and require less time to write programs. Third-generation programming languages are high-level programming languages, such as FORTRAN, Java, C, and C++.
Fourth Generation Programming Language — A fourth-generation language (4GL) is closer to a natural language (for example, English) than a third- generation language. These languages are non-procedural. It means that the programmer specifies what is required as opposed to how it is to be done. Database languages such as Structured Query Language (SQL), report generators such as Oracle Reports, and Python are examples of fourth-generation languages.
Fifth Generation Programming Language — A fifth-generation language (5GL) is designed to solve a given problem using constraints given to the program, rather than using an algorithm written by a programmer. The fifth-generation languages are mainly used in Artificial Intelligence. Smalltalk, Prolog, and Mercury are good examples of the fifth-generation languages.
Question 2
What are programming paradigms? Briefly explain two popular programming paradigms.

Answer

A programming paradigm is an approach or style of programming that is used to classify programming languages. Each programming language uses one or more programming paradigms. Two popular programming paradigms are:

Procedure Oriented Programming — In this programming paradigm, a complex programming problem is solved by dividing it into smaller problems using functions (or procedures).
Object Oriented Programming — In this programming paradigm, data and functions are wrapped into a single unit.
Question 3
What are the characteristics of procedural programming?

Answer

The characteristics of procedural programming are:

Procedural programming follows a top-down approach.
The program is divided into blocks of codes called functions, where each function performs a specific task.
Procedural programs model real-world processes as 'procedures' operating on 'data'.
The data and functions are detached from each other.
The data moves freely in a program.
It is easy to follow the logic of a program.
A function can access other function's data by calling that function.
Question 4
What are the limitations of procedural programming?

Answer

The limitations of procedural programming are:

Procedural programming mainly focuses on procedures or functions. Less attention is given to the data.
The data and functions are separate from each other.
Global data is freely moving and is shared among various functions. Thus, it becomes difficult for programmers to identify and fix issues in a program that originate due to incorrect data handling.
Changes in data types need to be carried out manually all over the program and in the functions using the same data type.
Limited and difficult code reusability.
It does not model real-world entities (e.g., car, table, bank account, loan) very well where we as a human being, perceive everything as an object.
The procedural programming approach does not work well for large and complex systems.
Question 5
Write a short note on Object Oriented Programming.

Answer

Object Oriented Programming (OOP) is a programming paradigm which revolves around the behaviour of an object, and its interactions with other objects and classes. In OOP, the program is organised around data or objects rather than functions or procedures. It follows the design principles of Data Abstraction, Encapsulation, Inheritance, and Polymorphism.

Question 6
Explain the phrase, "Everything is an object".

Answer

The world around us is made up of objects such as people, vehicles, buildings, streets, mobiles, television, etc. Each of these objects has the ability to perform specific actions and each of these actions influences the other objects in some way or the other. The objects around us can be divided into the following categories:

Tangible Objects — These are the objects that we can see and touch. For example, chair, pen, book, door, etc.

Conceptual Objects — These objects exists as a conceptual entity that we cannot touch. We may or may not be able to see them. For example, an email, a bank account, a song, patents, etc.

Roles — Roles played by people, such as a student, a teacher or a clerk.

Events — An event is something occurring in a system or an organisation. For example, a sale or a purchase in a departmental store, someone's birthday, etc.

Question 7
What are the characteristics of object-oriented programming?

Answer

The characteristics of object-oriented programming are:

It follows a bottom-up approach.
The program resulting from object-oriented programming is a collection of objects. Each object has its own data and a set of operations.
OOP restricts the free movement of data and the functions that operate on it.
A properly defined class can be reused, giving way to code reusability.
The concept of object-oriented programming models real-world entities very well.
Due to its object-oriented approach, it is extremely useful in solving complex problems.
Question 8
What are the limitations of object-oriented programming?

Answer

Limitations of object-oriented programming:

The size of the programs created using this approach may become larger than the programs written using procedure-oriented programming approach.
Software developed using this approach requires a substantial amount of pre-work and planning.
OOP code is difficult to understand if you do not have the corresponding class documentation.
In certain scenarios, these programs can consume a large amount of memory.
Question 9
What do you mean by Abstraction? Give suitable examples.

Answer

Abstraction refers to the act of representing essential features without including the background details. For example, a building can be viewed as a single component (e.g., hospital) rather than separate components like cement, bricks, and rods. Abstraction is relative to the perspective of the viewer.


```

# MCQ 3 : 

```c++
1. In OOP languages, all of the processing and functionality has been moved to

A.  the objects 
B.  the classes 
C.  class members 
D.  all of the above 
Hide Answer !

Answer : the objects.

2. What term in procedural languages corresponds closely with classes in OOPs?

A.  variable 
B.  function 
C.  system defined data type 
D.  user defined data type 
Hide Answer !

Answer : user defined data type.

3. What is the main purpose of creating an abstract base class ?

A.  creating the dynamic object 
B.  dynamic binding 
C.  deriving classes 
D.  none of these 
Hide Answer !

Answer : dynamic binding.

4. Object is

A.  collection of classes 
B.  collection of members 
C.  instance of class 
D.  similar to classes 
Hide Answer !

Answer : instance of class.

5. nheritance is useful for producing a software, which is

A.  instantaneous 
B.  unpredictable 
C.  reusable 
D.  none of these 
Hide Answer !

Answer : reusable.

6. A metaclass is a class

A.  whose instances themselves are classes 
B.  in which instance created will itself be a class 
C.  A and B 
D.  None of the above 
Hide Answer !

Answer : A and B.

7. The size of the object of the class is

A.  Sum of sizes of its data and function members 
B.  size of its largest data member 
C.  size of its largest function member 
D.  sum of sizes of its data members 
Hide Answer !

Answer : sum of sizes of its data members.

8. Is implementation of virtual function is efficient to achieve a goal as compared with implementation of non-virtual function?

A.  Depends upon implementation 
B.  It is efficient 
C.  Depends upon stack availability 
D.  It is not efficient 
Hide Answer !

Answer : It is not efficient.

9. Polymorphism means

A.  one class, multiple methods 
B.  multiple classes, multiple methods 
C.  one interface, multiple methods 
D.  multiple interfaces, multiple methods 
Hide Answer !

Answer : one interface, multiple methods.

10. Default arguments can be used to _________

A.  pass parameters to another function 
B.  increase execution speed 
C.  Reduce memory requirement 
D.  None of these 
Hide Answer !

Answer : None of these.

11. A dangling pointer is one

A.  which arises when you use the address of an object after its lifetime is over 
B.  which may occur in situations like returning addresses of the automatic variables from a function or using the address of the memory block after it is freed 
C.  Option 1 and Option 2 
D.  None of the above 
Hide Answer !

Answer : Option 1 and Option 2.

12. If a class is having two or more members of types of another classes, the relationship is called as ____________

A.  has – a relationship 
B.  is – a relationship 
C.  kind of relationship 
D.  part-of relationship 
Hide Answer !

Answer : part-of relationship.

13. What is the meaning of the statement. const Hello *p;

A.  p is a pointer to an object of class Hello that can modify the contents of object 
B.  p is a pointer to an object of class Hello that can access const member functions of object 
C.  p is a pointer to an object of class Hello that can access static member functions of object 
D.  p is a pointer to an object of class Hello that can access any member of object 
Hide Answer !

Answer : p is a pointer to an object of class Hello that can access const member functions of object.

14. Inheritance in OOPs represents _________

A.  a view from big to small 
B.  a view from generic to specific 
C.  A and B 
D.  none of the above 
Hide Answer !

Answer : a view from generic to specific.

15. After successful memory allocation, new returns __________

A.  NULL 
B.  Zero 
C.  void 
D.  None of these 
Hide Answer !

Answer : None of these.

16. When a derived class fails to override a virtual function, then

A.  The first redefinition found in reverse order of derivation is used 
B.  If the object of the derived class accesses that function, the function defined by the base class is used 
C.  Both, 1 and 2 are true 
D.  Both, 1 and 2 are false 
Hide Answer !

Answer : Both, 1 and 2 are true.

17. Virtual functions ____

A.  Must be friend of the base class 
B.  Must be static member of the base class which must be defined 
C.  Must be non-static member of the base class which must be defined 
D.  Must be static member of the base class which need not be defined 
Hide Answer !

Answer : Must be non-static member of the base class which must be defined.

18. What is true about virtual functions?

A.  Can be called from constructors but not from destructors 
B.  Can be called from destructors but not from constructors 
C.  Cannot be called from either constructors or from destructors 
D.  Can be called from both constructors and destructors 
Hide Answer !

Answer : Can be called from both constructors and destructors.

19. What is true in case of virtual function?

A.  A virtual function in base class must be defined, even though it may not be used 
B.  Static member functions can be made virtual 
C.  We cannot have virtual destructors 
D.  A virtual function cannot be a friend of another class 
Hide Answer !

Answer : A virtual function in base class must be defined, even though it may not be used.

20. A derived class inherits every member of a base class except

A.  its constructors and destructors 
B.  its operator = () members 
C.  its friends 
D.  all of the above 
Hide Answer !

Answer : all of the above.

21. Static members of a class

A.  means class variable 
B.  have the same properties as that of global variables but having class scope 
C.  A and B 
D.  A or B 
Hide Answer !

Answer : A and B.

22. Which statement is true in case of a destructor?

A.  A destructor can be overloaded 
B.  A destructor has to be called explicitly 
C.  A destructor does not return any value 
D.  A destructor can have parameters 
Hide Answer !

Answer : A destructor does not return any value.

23. Overloading of which operator will be similar to invoking a copy constructor?

A.  + 
B.  . (dot) 
C.  = 
D.  ~ (tilde) 
Hide Answer !

Answer : . (dot) --> wrong aanswer 
: right answer : = 

24. In which of the following cases is a copy constructor invoked?

A.  When a new object of a class is initialized with the existing object of that class 
B.  When a copy of an object is passed by value as an argument to a function 
C.  When you return an object of the class by value 
D.  All of the above 
Hide Answer !

Answer : All of the above.

25. When does dynamic binding occur?

A.  During construction 
B.  During compilation 
C.  During linking 
D.  During execution 
Hide Answer !

Answer : During execution.

26. Overriding causes _________

A.  base class function to be called 
B.  derived class function to be called 
C.  a virtual function to be called 
D.  first base class then derived class function to be called 
Hide Answer !

Answer : derived class function to be called.

27. With which of the following streams is the keyboard device associated by default?

A.  cerr 
B.  clog 
C.  cin 
D.  cout 
Hide Answer !

Answer : cin.

28. What is true about a virtual base class?

A.  Constructors for virtual base classes are invoked before any non-virtual base classes 
B.  A class whose constructors have parameters can be a virtual base class 
C.  Only one copy of the class is inherited by the next generation class 
D.  All of the above are true 
Hide Answer !

Answer : All of the above are true.

29. Static member functions _________

A.  can be used without an instantiation of an object 
B.  Can only access static data 
C.  A and B 
D.  Neither A nor B 
Hide Answer !

Answer : A and B.

30. Which of the following statements have errors?

A.  cout.width(10).precision(3); 
B.  cout << resetiosflags(ios::left,ios::showpoint); 
C.  cout << put(“siit”); 
D.  All of the above statements have errors 
Hide Answer !

Answer : All of the above statements have errors.

31. this pointer ____________

A.  addresses the class object for which the member function is called 
B.  points to addresses of member function 
C.  pointes to class object 
D.  none of the above 
Hide Answer !

Answer : addresses the class object for which the member function is called.

32. The inline specifier ________

A.  Does not change the behavior of a function 
B.  Tells the compiler to make a use of a stack for calling a function 
C.  1 and 2 
D.  None of the above 
Hide Answer !

Answer : Does not change the behavior of a function.

33. C++ is developed by

A.  Dennis Ritchie 
B.  Bjarne Stroustrup 
C.  Herbert Schitz 
D.  Bjarne Borg 
Hide Answer !

Answer : Bjarne Stroustrup.

34. The :: is called

A.  double dot 
B.  four sqr 
C.  delimiter 
D.  scope resolution operator 
Hide Answer !

Answer : scope resolution operator.

35. Iterators are

A.  pointer like entities used to access elements in a container 
B.  linked list structures 
C.  stacked pointers in vector map 
D.  None of the above 
Hide Answer !

Answer : pointer like entities used to access elements in a container.

36. volatile qualifier indicates that memory location

A.  can be altered 
B.  cannot be altered 
C.  will not be allocated 
D.  will disappear 
Hide Answer !

Answer : can be altered.

37. Data member of a class can be qualified as static

A.  True 
B.  False 
C.  Only static friends 
D.  Only void static 
Hide Answer !

Answer : True.

38. The private data of a class accessed by a friend function

A.  is in the object that invoked the friend 
B.  is in the object created by the friend function 
C.  is in the object of different class 
D.  is in the object sent to the friend function as argument 
Hide Answer !

Answer : is in the object sent to the friend function as argument.

39. The use of delete is to

A.  Only deallocate the memory allocated by new 
B.  Deallocate the memory allocated by new and call the class destructor 
C.  Call the destructor of that class whose object is to be destroyed 
D.  None of the above 
Hide Answer !

Answer : Deallocate the memory allocated by new and call the class destructor.

40. Character constants and string constants are same representation

A.  Characters can have numbers but string constants do not 
B.  Strings are nothing but character array 
C.  False 
D.  True 
Hide Answer !

Answer : False.

41. What is the main purpose of creating an abstract base class ?

A.  creating the dynamic object 
B.  dynamic binding 
C.  deriving classes 
D.  none of these 
Hide Answer !

Answer : dynamic binding.

42. The size of the object of the class is

A.  Sum of sizes of its data and function members 
B.  size of its largest data member 
C.  size of its largest function member 
D.  sum of sizes of its data members 
Hide Answer !

Answer : sum of sizes of its data members.

43. Is implementation of virtual function is efficient to achieve a goal as compared with implementation of non-virtual function?

A.  Depends upon implementation 
B.  It is efficient 
C.  Depends upon stack availability 
D.  It is not efficient 
Hide Answer !

Answer : It is not efficient.

44. When a derived class fails to override a virtual function, then

A.  The first redefinition found in reverse order of derivation is used 
B.  If the object of the derived class accesses that function, the function defined by the base class is used 
C.  Both, 1 and 2 are true 
D.  Both, 1 and 2 are false 
Hide Answer !

Answer : Both, 1 and 2 are true.

45. In C++, a function contained within a class is called ____________

A.  a member function 
B.  an operator 
C.  a class function 
D.  a method 
Hide Answer !

Answer : a member function.

46. Anonymous union means

A.  a union having no member 
B.  a union having no tag name 
C.  a union having only one object 
D.  none of the above 
Hide Answer !

Answer : a union having no tag name.

47. If a class D is derived from class A and class B is derived from class D, then it is called as

A.  multitple 
B.  hierarchical 
C.  muli-level 
D.  single 
Hide Answer !

Answer : muli-level.

48. Variables are to data types as

A.  members are to classes 
B.  data items are to classes 
C.  objects are to classes 
D.  data is to a variable 
Hide Answer !

Answer : objects are to classes.

49. In inheritance __________

A.  it doesn’t work in reverse 
B.  the base class and its objects have no knowledge about any classes derived from the base class 
C.  A and B 
D.  none of the above 
Hide Answer !

Answer : A and B.

50. The idea behind virtual functions is to _______ the derived class object’s address into a pointer to a base class object

A.  downcast 
B.  upcast 
C.  modify 
D.  none of the above 
Hide Answer !

Answer : upcast.


```

### What is inherited from the base class?
```c++
In principle, a publicly derived class inherits access to every member of a base class except:

its constructors and its destructor
its assignment operator members (operator=)
its friends
its private members

Even though access to the constructors and destructor of the base class is not inherited as such, they are automatically called by the constructors and destructor of the derived class.

Unless otherwise specified, the constructors of a derived class calls the default constructor of its base classes (i.e., the constructor taking no arguments). Calling a different constructor of a base class is possible, using the same syntax used to initialize member variables in the initialization list:

derived_constructor_name (parameters) : base_constructor_name (parameters) {...}
```